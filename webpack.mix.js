const mix = require('laravel-mix');
const path = require('path');

mix
   .setPublicPath('app/public/')
   .js('app/assets/scripts/app.js', 'js')
   .sass('app/assets/styles/app.scss', 'css')
   .version();
